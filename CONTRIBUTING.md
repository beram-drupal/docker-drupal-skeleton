# Contributing to this project

## Reporting Issues

When reporting issues, please try to be as descriptive as possible, and include
as much relevant information as you can. A step by step guide on how to
reproduce the issue will greatly increase the chances of your issue being
resolved in a timely manner.

## Contributing policy

### Git

**Commit message must:**

 * Be in English
 * Follow [this convention](http://karma-runner.github.io/0.10/dev/git-commit-msg.html)

**Useful commands:**

 * Use `git pull --rebase` and/or `git rebase {BRANCH}`.
   Careful! Do not rebase if you already have some commits on the remote server
   and someone else is working on this branch too.
 * To display git log nicely use `git log --graph --decorate --oneline`.
 * If the commit you want to fix isn’t the most recent one: `git rebase --interactive LAST_COMMIT_HASH_BEFORE_YOU_SCREW_THINGS_UP`
 * Squash commits : `git rebase --interactive hash`
 * Amend the last commit to remove an unwanted file from the commit: `git reset --soft HEAD~1`

## Update your Drupal app

After updated your git repository you need to update your Drupal App: cf. `Makefile`.

The safe sequence for sharing according to [Drupal Con Vienna 2017 Advanced Configuration Management with Config Split et al](#drupalcon-vienna-cmi):

 * Export configuration: `drush cex`
 * Commit: `git add` && `git commit`
 * Merge: `git pull`
 * Update dependencies: `composer`
 * Run updates: `drush updb`
 * Import configuration: `drush cim` 
 * Push: `git push`

**The important is to understand to use git to resolve conflict on the configuration and not Drupal.**

On this project it means:

 * Export configuration: `make drupal-config-export`
 * Commit: `git add` && `git commit`
 * Merge: `git pull`
 * Upgrade the project: `make project-upgrade`
 * Push: `git push`

## Further reading

 * Git
   * [Gitlab flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
   * [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
   * [A succesful Git branching model considered harmful](https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful/)
   * [A (simpler) successful Git branching model](http://drewfradette.ca/a-simpler-successful-git-branching-model/)
 * Composer
   * [Composer Cheat Sheet for developers](http://composer.json.jolicode.com/)
   * [Tips for Managing Drupal 8 projects with Composer](https://www.jeffgeerling.com/blog/2017/tips-managing-drupal-8-projects-composer)
 * Drush
   * [Drush commands](http://drushcommands.com)<a name="drush-commands"/>
 * Configuration Management
   * [Configuration API overview](https://www.drupal.org/node/1667894)
   * [Advanced Drupal 8 Configuration Management (CMI) Workflows](https://blog.liip.ch/archive/2017/04/07/advanced-drupal-8-cmi-workflows.html)
   * [Drupal Con Vienna 2017 Advanced Configuration Management with Config Split et al.](https://events.drupal.org/sites/default/files/slides/advanced_CM-config_split-DCvienna2017.pdf)<a name="drupalcon-vienna-cmi"/>
 * PHP
   * [Clean Code PHP](https://github.com/jupeter/clean-code-php)
   * [Stories about good software architecture and how to create it with design patterns](https://sourcemaking.com/)
