<?php

declare(strict_types=1);

namespace Drush\Generators;

use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class HookHandlerGenerator.
 *
 * @package Drush\Generators
 */
class HookHandlerGenerator extends BaseGenerator {

  /**
   * The default name for the main method of the generated class.
   */
  private const HOOK_DEFAULT_METHOD = 'process';

  /**
   * A map between hook and main method of the generated class.
   *
   * Key: needle to identify the hook.
   * Value: name of the main method of the generated class.
   */
  private const HOOK_METHOD_MAP = [
    'preprocess' => 'preprocess',
    'alter' => 'alter',
  ];

  /**
   * {@inheritdoc}
   */
  protected $name = 'hook-handler';

  /**
   * {@inheritdoc}
   */
  protected $description = 'Generates a hook handler.';

  /**
   * {@inheritdoc}
   */
  protected $templatePath = __DIR__ . '/templates';

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions = Utils::defaultQuestions();

    // @TODO: autocomplete values could be great.
    $questions['hook'] = new Question('Hook name (part after "hook_")', 'preprocess_node');
    $questions['hook']->setNormalizer(function ($answer) {
      return Utils::human2machine($answer);
    });

    // @TODO: default value based on the hook could be great.
    $questions['hook_type'] = new Question(
      'Hook type (Preprocess, Form, PageAttachments, ThemeSuggestions etc..)',
      'Preprocess')
    ;
    $questions['hook_type']->setNormalizer(function ($answer) {
      return  Utils::camelize($answer, TRUE);
    });

    $this->collectVars($input, $output, $questions);

    $this->vars['hook_method'] = $this->getHookMethod($this->vars['hook']);

    $defaultClass = sprintf('%sHookHandler', Utils::camelize($this->vars['hook'], TRUE));
    $classQuestion = new Question('Hook handler class name', $defaultClass);
    $classQuestion->setValidator([Utils::class, 'validateClassName']);
    $this->vars['class'] = $this->ask($input, $output, $classQuestion);

    $diQuestion = new ConfirmationQuestion('Would you like to inject dependencies?');
    if ($this->ask($input, $output, $diQuestion)) {
      $this->collectServices($input, $output);
    }

    $this->addFile()
      ->path('src/HookHandler/{hook_type}/{class}.php')
      ->template('hook-handler.twig');
  }

  private function getHookMethod(string $hook): string {
    foreach (self::HOOK_METHOD_MAP as $id => $action) {
      if (FALSE === strpos($hook, $id)) {
        continue;
      }

      return $action;
    }

    return self::HOOK_DEFAULT_METHOD;
  }

}
