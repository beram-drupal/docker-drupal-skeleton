<?php

/**
 * @file
 * Contains Redis configuration.
 */

$settings['redis.connection']['interface'] = 'PhpRedis';
$settings['redis.connection']['host'] = 'redis';
$settings['redis.connection']['port'] = 6379;
$settings['redis.connection']['base'] = 0;
$settings['cache_prefix'] = getenv('PROJECT_NAME') . '_';

// Additional redis services.
$settings['container_yamls'][] = 'modules/contrib/redis/redis.services.yml';
$settings['container_yamls'][] = 'modules/contrib/redis/example.services.yml';

// Use redis cache backend for all bins otherwise specified.
$settings['cache']['default'] = 'cache.backend.redis';
// Always set the fast backend for bootstrap, discover and config, otherwise
// this gets lost when redis is enabled.
// @see https://api.drupal.org/api/drupal/core%21core.api.php/group/cache/8.5.x
$settings['cache']['bins']['bootstrap'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['discovery'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['config'] = 'cache.backend.chainedfast';
