#!/bin/sh

set -eu

scriptdir=$( dirname "$0" )
# shellcheck source=scripts/lib/_messages.sh
. "${scriptdir}"/lib/_messages.sh

usage(){
    printf "\\n"
    printf "Usage:\\n\\t%s URI\\n" "$0"
    printf "\\n"
    exit 0
}

if [ -z "${1+x}" ] || [ "$1" = '-h' ]; then
    usage
fi

uri=$1

msg_info "Start upgrade ${uri}..."

msg_info "Update Database..."
drush --uri="${uri}" updatedb --yes
drush --uri="${uri}" cr

msg_info "Update Config..."
drush --uri="${uri}" config-import --yes
drush --uri="${uri}" cr
# Sometimes Drupal does not update all config
# so we need to reimport it to be sure.
drush --uri="${uri}" config-import --yes
drush --uri="${uri}" cr

msg_info "Update Translation..."
drush --uri="${uri}" locale-check
drush --uri="${uri}" locale-update

msg_info "Rebuild Cache..."
drush --uri="${uri}" cr

msg_info "End upgrade ${uri}."
