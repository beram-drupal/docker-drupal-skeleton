#!/bin/sh

set -eu

scriptdir=$( dirname "$0" )
# shellcheck source=scripts/lib/_messages.sh
. "${scriptdir}"/lib/_messages.sh

usage(){
    printf "\\n"
    printf "Usage:\\n\\t%s [-u URI] [-m MODE] [-d DRUPAL_ROOT_DIRECTORY]\\n" "$0"
    printf "\\n"
    printf "Arguments:\\n"
    printf "\\t-u : The Drupal site uri. Default to 'default'\\n"
    printf "\\t-m : The mode: [dev,prod]. Default to 'prod'\\n"
    printf "\\t-d : The Drupal root directory. Default to '/var/www/html/src'"
    printf "\\n"
    exit 0
}

check_site_directory(){
    if [ ! -d "${drupal_root_directory}/sites/${uri}" ]; then
        msg_error "There is no site for the provided uri.";
        exit 1;
    fi
}

mode_prod(){
    rm "${drupal_root_directory}"/sites/"${uri}"/settings.local.php
    drush --uri="${uri}" cr
}

mode_dev(){
    cp -v "${drupal_root_directory}"/sites/example.settings.local.php "${drupal_root_directory}"/sites/"${uri}"/settings.local.php
    drush --uri="${uri}" cr
}

main(){
    case "${mode}" in
        prod)
            mode_prod
            ;;
        dev)
            mode_dev
            ;;
        *)
            msg_error "There is no mode ${mode}"
            exit 1
            ;;
    esac

    msg_info "End changing to use the '${mode}' mode for site '${uri}'."
}

if [ $# -eq 0 ]; then
    usage
fi

# Default values.
uri=default
mode=prod
drupal_root_directory=/app/app

# Process options.
while getopts u:m:d:h VARNAME; do
    case "${VARNAME}" in
        h)
            usage
            ;;
        u)
            uri="${OPTARG}"
            ;;
        m)
            mode="${OPTARG}"
            ;;
        d)
            drupal_root_directory="${OPTARG}"
            ;;
        \?)
            usage
            ;;
    esac
done

check_site_directory

main
