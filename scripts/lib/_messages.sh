#!/bin/sh

msg_error(){
    printf "[ERROR] %s \\n" "$1";
}

msg_warning(){
    printf "[WARNING] %s \\n" "$1";
}

msg_info(){
    printf "[INFO] %s \\n" "$1";
}
