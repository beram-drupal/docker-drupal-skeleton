# For the Drupal PHP requirements see https://www.drupal.org/docs/8/system-requirements/drupal-8-php-requirements
FROM php:7.4-fpm-alpine3.11

ARG DRUSH_LAUNCHER_VERSION=0.6.0
ARG XDEBUG_VERSION=2.9.4

ENV TERM xterm
ENV REDIS_HOST=redis
ENV REDIS_VERSION=5.2.1
# Default host for MacOS.
# For Linux use the IP address of docker0 interface.
ENV XDEBUG_REMOTE_HOST=host.docker.internal

RUN apk add --update --upgrade apk-tools alpine-sdk \
    autoconf \
    bash \
    curl \
    gdb \
    htop \
    icu-dev \
    libxml2 \
    make \
    git \
    php7-intl \
    postgresql-dev \
    postgresql-client \
    pcre-dev \
    strace \
    tzdata \
    vim \
    zip \
    patch \
    jpeg-dev \
    libpng \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp-dev \
    zlib-dev \
    libxpm-dev \
    freetype-dev \
    gettext \
    file \
    parallel \
    ssmtp

RUN deluser www-data \
    && adduser -D -g 'php user' -h /var/www -s /bin/false www-data

# Install/configure php ext
RUN pecl install redis-${REDIS_VERSION} && docker-php-ext-enable redis \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install intl \
    && docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include/freetype2 && docker-php-ext-install gd \
    && pecl install xdebug-${XDEBUG_VERSION} \
    && docker-php-ext-enable xdebug

# Delete useless things to dev
RUN docker-php-source delete \
    && apk del --purge alpine-sdk autoconf pcre-dev tzdata \
    && rm -rf /usr/share/vim/vim74/doc/* /usr/share/vim/vim74/tutor/* /usr/src/php.tar* /var/cache/apk/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer global require hirak/prestissimo

# Install Drush Launcher
RUN drush_launcher_url="https://github.com/drush-ops/drush-launcher/releases/download/${DRUSH_LAUNCHER_VERSION}/drush.phar" \
    && wget -O drush.phar "${drush_launcher_url}" \
    && chmod +x drush.phar \
    && mv drush.phar /usr/local/bin/drush

COPY ./php.template.ini /tmp/php.template

CMD /bin/sh -c "envsubst '\$REDIS_HOST \$XDEBUG_REMOTE_HOST' < /tmp/php.template > /usr/local/etc/php/conf.d/php.ini && php-fpm"

WORKDIR /app
