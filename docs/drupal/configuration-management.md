# Configuration Management <a name="config-mgmtn"/>

This project uses CMI and [Config Split](https://www.drupal.org/project/config_split)
module to handle configurations.

For more information [look at the Further Reading section](#drupalcon-vienna-cmi).

## Environment Specific Configurations

Specific configurations per environment are handle with
[Config Split](https://www.drupal.org/project/config_split) module.

Each environment has its own "split" if necessary.
To activate it, on the `settings.php` check that the split is active:

```
$config['config_split.config_split.mysplit']['status'] = TRUE;
```

## Ignored Configurations

To allow the client to contribute some specific part, the concerned configurations
are ignored using the [Config Ignore](https://www.drupal.org/project/config_ignore) module.

If you need default values to be installed you need to have the configuration yml file
placed in the synchronization directory and you need to commit it.

If this file is ignored by [Config Ignore](https://www.drupal.org/project/config_ignore) module
it will be installed only during the module installation with `drush cim`.

During the module installation process, the files under `mymodule/config` are ignored,
only the files under the `sync` directory are taken into account.

## Further reading

 * [Configuration API overview](https://www.drupal.org/node/1667894)
 * [Advanced Drupal 8 Configuration Management (CMI) Workflows](https://blog.liip.ch/archive/2017/04/07/advanced-drupal-8-cmi-workflows.html)
 * [Drupal Con Vienna 2017 Advanced Configuration Management with Config Split et al.](https://events.drupal.org/sites/default/files/slides/advanced_CM-config_split-DCvienna2017.pdf)<a name="drupalcon-vienna-cmi"/>
