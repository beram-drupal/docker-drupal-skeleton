# Drupal 8 Theming Guide

This theming guide is a reference of various techniques and snippets which are
typically needed for front-end development scenarios on a project.
Some documentation will reference high-level techniques that are inline
with best practices, while others are specific code from certain tasks.  

Some parts of this theming guide are inspired by
[Acquia COG Drupal Theme StarterKit](https://github.com/acquia-pso/cog/tree/8.x-1.x/STARTERKIT/_theming-guide).
