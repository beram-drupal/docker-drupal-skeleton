# Docker Drupal Skeleton

[![pipeline status](https://gitlab.com/beram-drupal/docker-drupal-skeleton/badges/8.1.x/pipeline.svg)](https://gitlab.com/beram-drupal/docker-drupal-skeleton/commits/8.1.x)

This Drupal 8 Project Skeleton allows you to easily start a new Drupal8 project.

For an example project you could check https://gitlab.com/beram-drupal/drupal-ci

## Requirements

 * [Docker](https://docs.docker.com/)
 * [Docker Compose](https://docs.docker.com/compose/overview/)
 * [Git](https://git-scm.com/)

## Installation

Clone the repository:

```bash
$ git clone $REPOSITORY
```

Go to your project folder and configure your environment by editing your .env:

```bash
$ cp .env.dist .env
$ vi .env # Edit as you whish with your favorite editor
```

> You could modify the `docker-composer.yml` file to suit your architecture requirements ;)

Check available `make` commands:

```bash
$ make help
```

To just build the container, run composer install/scaffold and install a vanilla drupal:

```bash
$ make project-install-vanilla
$ make drupal-enable-default-modules # This is optional but recommended.
```

Remember to clean, delete, configure whatever you want the way you want.

## Documentation

 * [Contributing guide](CONTRIBUTING.md)
 * [Drupal guide](docs/drupal)
 * [Redis guide](docs/redis)

## TODOs

- [ ] Add documentation
- [ ] Add support for Varnish
- [ ] Provide default configuration for phpunit
- [ ] Add CI
